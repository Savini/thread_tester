#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define FIN "input.txt"
#define FOUT "output.txt"
#define RES "result.txt"

#define DEB printf("line %d\n", __LINE__);

void help(void)
{
	printf("\n");
	printf("Usage:\n\n");
	printf("		./tester.exe [r]:[c] [threads]\n\n");
	printf("		r: number of rows\n");
	printf("		c: number of columns\n");
	printf("		threads: max number of threads\n");
	printf("\n\n");
}


int main(int argc, char *argv[])
{
	//int rmax, cmax;
	int i, j, r, c;
	int th_num = 1;
	clock_t start, stop;
	FILE *fi, *fo, *fr;

	if (argc != 3/*(sscanf(argv[1], "%d:%d", &r, &c) != 2) && (sscanf(argv[2], "%d", &th_max) != 1)*/) {
		help();
		return 1;
	}

	
	if (sscanf(argv[1], "%d:%d", &r, &c) != 2) {
		help();
		return 1;
	}

	sscanf(argv[2], "%d", &th_num);

//	sscanf(argv[1], "%d:%d", &r, &c);
//	sscanf(argv[2], "%d", &th_max);

	int *mat = (int *)malloc(r*c * sizeof(int));
	char *line = (char *)malloc((c + 2) * sizeof(char));

	fo = fopen(FOUT, "w");


	fi = fopen(FIN, "w");

	for (i = 0; i < r; i++) {
		for (j = 0; j < c; j++) {
			fprintf(fi, "%d ", i*c + j);
		}
		fprintf(fi, "\n");
	}

	fclose(fi);

	fr = fopen(RES, "a");
	fi = fopen(FIN, "r");
	
//	fprintf(fr, "dim %d:%d\nthreads %d\n", r, c, th_num);

//	for (; th_num < th_max; th_num++) {
		start = clock();
		omp_set_num_threads(th_num);
#pragma omp parallel 
		{
#pragma omp for schedule(static)

			for (i = 0; i < r; i++) {
				for (j = 0; j < c; j++) {
					mat[i*c + j] = i*c + j;//omp_get_thread_num();
				}
			}
//			printf("#threads: %d\n", omp_get_num_threads());
		}

		stop = clock();
DEB
#pragma omp parallel
		{
#pragma omp for schedule(static)
			
			for (i = 0; i < r; i++) {
				for (j = 0; j < c; j++) {
					fprintf(fo, "%d ", mat[i*c + j]);
				}
				fprintf(fo, "\n");
			}
//		printf("#threads: %d\n", omp_get_num_threads());
		}

		
#pragma omp parallel
		{
#pragma omp for schedule(static)
		
			for (i = 0; i < r; i++) {
				fgets(line, sizeof(line), fi);
				for (j = 0; j < c; j++) {
					mat[i*c + j] = line[j];
				}
			}
//		printf("#threads: %d\n", omp_get_num_threads());
		}

		

	

	fprintf(fr, "%dx%d, %d, %lf\n", r, c, th_num, (double)((double)(stop - start) / CLOCKS_PER_SEC));

//	}

	fprintf(fr, "\n\n");
	
	fclose(fo);
	fclose(fi);
	fclose(fr);
	free(mat);
	free(line);

	return 0;
}